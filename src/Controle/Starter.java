package Controle;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import Modele.*;

public class Starter {
    public static void main(String[] args) throws Exception {
        System.out.print("Veuillez indiquer votre choix :\n" +
                "med : acces aux medecins\n" +
                "prod : acces aux produits \n" +
                "rapp : acces aux rapport \n" +
                "login : se connecte\n" +
                "quit : pour arrêter le programme\n");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String text = in.readLine(); /*Récupère le choix*/
        String Nom;
        String id;
        String Consulter;
        String leRapport;
        String commentaire;
        Post poste = new Post();
        switch (text) {/*Menu de choix*/
            case "med":
                System.out.print("Veuillez indiquer votre choix :\n" +
                        "medecin : fiche d'un medecin\n" +
                        "liste : acces a la liste de medecins\n" +
                        "quit : pour arreter le programme\n");
                in = new BufferedReader(new InputStreamReader(System.in));
                text = in.readLine();
                switch (text) {/*Menu de choix*/
                    case "liste":
                        Consulter = "ListMed";
                        List<Medecin> Liste = poste.getMed(Consulter, null);
                        for (Medecin medecin: Liste){
                            System.out.println(medecin.toString());
                        }
                        break;
                    case "medecin":
                        Consulter = "Med";
                        System.out.println("Donnez le nom du medecin");
                        in = new BufferedReader(new InputStreamReader(System.in));
                        Nom = in.readLine();
                        List<Medecin> ListeCP = poste.getMed(Consulter, Nom);
                        for (Medecin medecin: ListeCP){
                            System.out.println(medecin.toString());
                        }

                        break;
                    case "quit":
                        System.out.println("Au revoir !");
                        return;
                    default:
                        System.out.println("Vous n'avez pas donner d'argument valable");
                        main(null);
                }
                break;
            case "login":
                String login;
                String pass;
                System.out.println("Donnez votre login");
                in = new BufferedReader(new InputStreamReader(System.in));
                login = in.readLine();
                System.out.println("Donnez votre pass");
                in = new BufferedReader(new InputStreamReader(System.in));
                pass = in.readLine();
                Visiteur myvisiteur = new Visiteur();
                myvisiteur.seConnecter(login,pass);
                System.out.println(myvisiteur.toString());
                break;
            case "prod":
                System.out.print("Veuillez indiquer votre choix :\n" +
                        "produit : affiche un produit\n" +
                        "liste : acces a la liste de produits\n" +
                        "quit : pour arreter le programme\n");
                in = new BufferedReader(new InputStreamReader(System.in));
                text = in.readLine();
                switch (text) {/*Menu de choix*/
                    case "liste":
                        Consulter = "ListProd";
                        List<Produit> Liste = poste.getProd(Consulter, null);
                        for (Produit prod : Liste) {
                            System.out.println(prod.toString());
                        }
                        break;
                    case "produit":
                        Consulter = "Prod";
                        System.out.println("Donnez le nom de votre produit");
                        in = new BufferedReader(new InputStreamReader(System.in));
                        Nom = in.readLine();
                        List<Produit> uneListe = poste.getProd(Consulter,Nom);
                        for (Produit unProd: uneListe){
                            System.out.println(unProd.toString());
                        }
                        break;
                    case "quit":
                        System.out.println("Au revoir !");
                        return;
                    default:
                        System.out.println("Vous n'avez pas donner d'argument valable");
                        main(null);
                }
                break;
            case "rapp":
                System.out.print("Veuillez indiquer votre choix :\n" +
                        "write : permet d'envoyer un rapport\n" +
                        "rapport : affichage d'un rapport\n" +
                        "liste : acces a la liste des rapports\n" +
                        "quit : pour arreter le programme\n");
                in = new BufferedReader(new InputStreamReader(System.in));
                text = in.readLine();
                switch (text) {/*Menu de choix*/
                    case "write":
                        System.out.println("entrer votre rapport");
                        in = new BufferedReader(new InputStreamReader(System.in));
                        leRapport = in.readLine();
                        System.out.println("entrer votre commentaire");
                        in = new BufferedReader(new InputStreamReader(System.in));
                        commentaire = in.readLine();
                        String result = poste.setRapp("dandre" , leRapport , commentaire);
                        System.out.println(result);

                        break;
                    case "liste":
                        Consulter = "ListRapp";
                        List<Rapport> Liste = poste.getRapp(Consulter, "" , "dandre");
                        for (Rapport unRapp: Liste)
                        {
                            System.out.println(unRapp.toString());
                        }
                        break;
                    case "rapport":
                        Consulter = "Rapp";
                        System.out.println("Donnez l'id du rapport � consulter");
                        in = new BufferedReader(new InputStreamReader(System.in));
                        id = in.readLine();
                        List<Rapport> uneListe = poste.getRapp(Consulter, id , "dandre");
                        for (Rapport unRapp: uneListe)
                        {
                            System.out.println(unRapp.toString());
                        }

                        break;
                    case "quit":
                        System.out.println("Au revoir !");
                        return;
                    default:
                        System.out.println("Vous n'avez pas donner d'argument valable");
                        main(null);
                }
                break;
            case "quit":
                System.out.println("Vous avez quitter le programme");
                break;
            default:
                System.out.println("Vous n'avez pas choisis un bon argument");
                main(null);
        }

    }
}