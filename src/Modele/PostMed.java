package Modele;
import Modele.Parser.Parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class PostMed {
    private String request;
    private String answer;

    public PostMed(String Methode,String med) throws Exception {
        this.request = "consulter="+ Methode + "&med=" + med;
        this._sendPost();
    }

    private void _sendPost() throws Exception {
        /*Connexion au site*/
        String url = "http://tomcat9.gsb-alpha.lan:8080/gsb/projet";
        URL obj = new URL(url);
        HttpURLConnection site = (HttpURLConnection) obj.openConnection();
        /*Setup de la methode POST*/
        System.out.println("Mise en place de la methode POST");
        site.setRequestMethod("POST");
        site.setRequestProperty("User-Agent", "Mozilla/5.0");
        site.setRequestProperty("Accept-Language","UTF-8");
        site.setDoOutput(true);
        OutputStreamWriter out = new OutputStreamWriter(site.getOutputStream());
        out.write(this.request);
        out.flush();
        System.out.println("Envoie d'une requête 'POST' à l'URL : " + url);
        int responseCode = site.getResponseCode();
        System.out.println("Paramètre du post : " + this.request);
        System.out.println("Code renvoyé : " + responseCode);
        BufferedReader in = new BufferedReader(new InputStreamReader(site.getInputStream()));
        StringBuffer response = new StringBuffer();

        String inputLine;
        while((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();
        this.answer = response.toString();
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getAnswer() {
        return this.answer;
    }

    public List<Medecin> getTraiter() throws IOException {
        List<Medecin> maListe = new LinkedList<Medecin>();
        Parser parse = new Parser(answer);
        List<String[]> reponse = parse.getParsed();
        for (int i = 0; reponse.size() > i; i++) {
            maListe.add(new Medecin(Integer.parseInt(reponse.get(i)[0]),Integer.parseInt(reponse.get(i)[6]), reponse.get(i)[1], reponse.get(i)[2], reponse.get(i)[3], reponse.get(i)[4], reponse.get(i)[5], reponse.get(i)[7]));
        }
        return maListe;
    }
}
