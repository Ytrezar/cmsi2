package Modele;

public class Rapport
{
    private String id;
    private String auteur, rapport, commentaire;

    public Rapport(String id, String auteur, String rapport, String commentaire)
    {
        this.id = id;
        this.auteur = auteur;
        this.rapport = rapport;
        this.commentaire = commentaire;
    }

    public String getId() {
        return id;
    }
    public String getAuteur() {
        return auteur;
    }
    public String getRapport() {
        return rapport;
    }
    public String getCommentaire() {
        return commentaire;
    }
    public void setId(String id) {
        this.id = id;
    }
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }
    public void setRapport(String rapport) {
        this.rapport = rapport;
    }
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    public void show(){/*Affiche les informations du rapport*/
        System.out.println(id + " " + commentaire);
    }


    public String toString(){
        return "Produit [id = " + this.id + ", Auteur =" + this.auteur + ", Rapport = "+ this.rapport +
                ", Commentaire = " + this.commentaire + "]";
    }
}