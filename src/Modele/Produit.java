package Modele;


import javax.swing.*;

public class Produit {
    private ImageIcon Photo;
    private String Nom, Description, Posologie, Notice, Ref;
    public void show(){/*Affiche les informations du produit*/
        System.out.println(Ref + " " + Nom + " " + Description);
    }
    public String getNom() {
        return Nom;
    }
    public void setNom(String nom) {
        Nom = nom;
    }
    public String getDescription() {
        return Description;
    }
    public void setDescription(String description) {
        Description = description;
    }
    public String getPosologie() {
        return Posologie;
    }
    public void setPosologie(String posologie) {
        Posologie = posologie;
    }
    public String getNotice() {
        return Notice;
    }
    public void setNotice(String notice) {
        Notice = notice;
    }
    public String getRef() {
        return Ref;
    }
    public void setRef(String ref) {
        Ref = ref;
    }


    public String toString(){
        return "Produit [Nom = " + this.Nom + ", Description =" + this.Description + ", Posologie = "+ this.Posologie +
                ", Notice = " + this.Notice + ", ref =" + this.Ref + "]";
    }

    public Produit(String Nom, String Description, String Posologie, String Notice, String Ref, ImageIcon Photo){
        this.Nom = Nom;
        this.Description = Description;
        this.Posologie = Posologie;
        this.Notice = Notice;
        this.Ref = Ref;
        this.Photo = Photo;
    }
}
