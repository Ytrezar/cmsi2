package Modele;

import Modele.Parser.Parser;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class PostProd {
    private String request;
    private String answer;
    public PostProd(String Methode, String nom) throws Exception {
        /*Vérification si on demande la liste ou un produit en particulier*/
        if (nom == null)
            request = "consulter="+Methode;
        else
            request = "consulter="+Methode+"&Prod="+nom;
        _sendPost();
    }
    private void _sendPost() throws Exception {

        /*Connexion au site*/
        String url = "http://tomcat9.gsb-alpha.lan:8080/gsb/projet";
        URL obj = new URL(url);
        HttpURLConnection site = (HttpURLConnection) obj.openConnection();

        /*Setup de la methode POST*/
        site.setRequestMethod("POST");
        site.setRequestProperty("User-Agent", "Mozilla/5.0");
        site.setRequestProperty("Accept-Language","UTF-8");
        site.setDoOutput(true);

        /*Envoi de la requête*/
        System.out.println("Mise en place de la methode POST");
        OutputStreamWriter out = new OutputStreamWriter(site.getOutputStream());
        out.write(request);
        out.flush();
        System.out.println("Envoie d'une requête 'POST' à l'URL : " + url);

        /*Récupération de code retour (A définir les erreurs eventuelles)*/
        int responseCode = site.getResponseCode();
        System.out.println("Paramètre du post : " + request);
        System.out.println("Code renvoyé : " + responseCode);

        /*Reception du message retour*/
        BufferedReader in = new BufferedReader(new InputStreamReader(site.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        answer = response.toString();
    }

    public String getAnswer() {
        return answer;
    }

    public List<Produit> getTraiter() throws IOException {
        List<Produit> maListe = new LinkedList<Produit>();
        Parser parse = new Parser(answer);
        List<String[]> reponse = parse.getParsed();
        for (String[] aReponse : reponse) {
            URL url = new URL(aReponse[5]);
            BufferedImage c = ImageIO.read(url);
            ImageIcon image = new ImageIcon(c);
            maListe.add(new Produit(aReponse[1], aReponse[2], aReponse[3], aReponse[4], aReponse[0], image));
        }
        return maListe;
    }
}