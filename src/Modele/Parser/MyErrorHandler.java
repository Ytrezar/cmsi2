package Modele.Parser;

import org.xml.sax.*;

import java.nio.charset.Charset;
import java.io.*;


public class MyErrorHandler implements ErrorHandler
{
    static PrintWriter out = new PrintWriter(new OutputStreamWriter(
            System.out, Charset.forName("CP850")), true);

    public void warning(SAXParseException e) throws SAXException {
        out.println ("** Analyse -- avertissement"
                + ", line " + e.getLineNumber () + "   " + e.getMessage ());
    }

    public void error(SAXParseException e) throws SAXException {
        out.println ("** Analyse -- erreur"
                + ", line " + e.getLineNumber () + "   " + e.getMessage ());
    }

    public void fatalError(SAXParseException e) throws SAXException {
        out.println ("** Analyse -- erreur fatale"
                + ", line " + e.getLineNumber () + "   " + e.getMessage ());
    }


}
