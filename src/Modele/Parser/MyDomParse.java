package Modele.Parser;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;


public class MyDomParse {
    List<String> list = new LinkedList<String>();
    public  Document LireDocument(String xmlFile)
    {
        Document document = null;

        try
        {

            DocumentBuilderFactory Factory = DocumentBuilderFactory.newInstance();
            //Factory.setValidating(false);
            DocumentBuilder Builder = Factory.newDocumentBuilder();
            Builder.setErrorHandler (new MyErrorHandler());
            document = Builder.parse(xmlFile);
        }
        catch (Throwable throwable) {
            throwable.printStackTrace();
            System.out.println("oups");
        }
        return document;

    }

    public List<String> getList() {
        return list;
    }

    public void parcourir(Element e)
    {
        NamedNodeMap nnm = e.getAttributes();
        for(int i=0;i<nnm.getLength();i++)
        {
            Node n = nnm.item(i);
        }
        NodeList nl = e.getChildNodes();
        for(int i=0;i<nl.getLength();i++)
        {
            Node n = nl.item(i);
            if (n.getNodeType()==Node.ELEMENT_NODE)
            {
                parcourir( (Element) n);
            } else
            if (n.getNodeType()==Node.TEXT_NODE && Objects.equals(e.getNodeName(), "p"))
            {
                list.add(n.getNodeValue());
            }
        }
    }
}
