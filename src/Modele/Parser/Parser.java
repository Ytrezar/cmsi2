package Modele.Parser;

import org.w3c.dom.Document;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class Parser {
    private String original;
    private ArrayList<String[]> parsed = new ArrayList<String[]>();


    public Parser(String answer){
        original = answer;
        _parsing();
    }


    private void _parsing() {
        /*Split les lignes dans un tableau*/
        /*Création d'un fichier xml pour contenir le texte*/
        PrintWriter out = null;
        try {
            out = new PrintWriter("original.xml");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        out.println(original);
        out.close();
        /*Création d'un objet Dom pour parcourir le html*/
        MyDomParse validateur = new MyDomParse();
        Document d = validateur.LireDocument("original.xml");
        validateur.parcourir(d.getDocumentElement());
        /*récupération de la liste triée et transformation en array*/
        LinkedList<String> list = (LinkedList<String>) validateur.getList();
        String[] separate = new String[list.size()];
        for(int i = 0; i < list.size(); i++) separate[i] = list.get(i);
        int i = 0;
        /*Split la ligne suivant le séparateur ";" et on vérifie que la ligne soit
        un element du tableau et non une balise html*/
        while (i < separate.length){
            parsed.add(separate[i].split("##"));
            i++;
        }
    }


    public List<String[]> getParsed() {
        return parsed;
    }
}
