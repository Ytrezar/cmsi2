package Modele;

public class Medecin {
    private Integer id;
    private Integer cp;
    private String nom;
    private String prenom;
    private String fonction;
    private String telephone;
    private String adresse;
    private String ville;

    public Medecin(Integer id, Integer cp, String nom, String prenom, String fonction, String telephone, String adresse, String ville) {
        this.nom = nom;
        this.prenom = prenom;
        this.fonction = fonction;
        this.telephone = telephone;
        this.adresse = adresse;
        this.ville = ville;
        this.cp = cp;
        this.id = id;
    }

    public String toString() {
        return "Medecin [id=" + this.id + ", cp=" + this.cp + ", nom=" + this.nom + ", prenom=" + this.prenom + ", fonction=" + this.fonction + ", telephone=" + this.telephone + ", adresse=" + this.adresse + ", ville=" + this.ville + "]";
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCp() {
        return this.cp;
    }

    public void setCp(Integer cp) {
        this.cp = cp;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getFonction() {
        return this.fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return this.ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}
