package Modele;

import Modele.Parser.Parser;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;

public class Post {
    private String request;
    private String answer;

    public Post() {
    }

    private void _sendPost() throws Exception {
        /*Connexion au serveur*/
        String url = "http://tomcat9.gsb-alpha.lan:8080/gsbv2/appli_Visiteur";
        URL obj = new URL(url);
        HttpURLConnection site = (HttpURLConnection) obj.openConnection();

        /*Setup de la methode POST*/
        site.setRequestMethod("POST");
        site.setRequestProperty("User-Agent", "Mozilla/5.0");
        site.setRequestProperty("Accept-Language", "UTF-8");
        site.setDoOutput(true);

        /*Envoi de la requête*/
        System.out.println("Mise en place de la methode POST");
        OutputStreamWriter out = new OutputStreamWriter(site.getOutputStream());
        out.write(request);
        out.flush();
        System.out.println("Envoie d'une requête 'POST' à l'URL : " + url);

        /*Récupération de code retour (A définir les erreurs eventuelles)*/
        int responseCode = site.getResponseCode();
        System.out.println("Paramètre du post : " + request);
        System.out.println("Code renvoyé : " + responseCode);

        /*Reception du message retour*/
        BufferedReader in = new BufferedReader(new InputStreamReader(site.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        answer = response.toString();
    }

    public List<Medecin> getMed(String Consulter, String Nom) throws Exception {
        this.request = "action=read&consulter="+Consulter+"&med="+Nom;
        this._sendPost();
        LinkedList<Medecin> maListe = new LinkedList<>();
        Parser parse = new Parser(this.answer);
        List<String[]> reponse = parse.getParsed();
        for (String[] aReponse : reponse) {
            maListe.add(new Medecin(Integer.parseInt(aReponse[0]), Integer.parseInt(aReponse[6]), aReponse[1], aReponse[2], aReponse[3], aReponse[4], aReponse[5], aReponse[7]));
        }
        return maListe;
    }
    public List<Produit> getProd(String Consulter, String Nom) throws Exception {
        this.request = "action=read&consulter="+Consulter+"&Prod="+Nom;
        this._sendPost();
        List<Produit> maListe = new LinkedList<>();
        Parser parse = new Parser(answer);
        List<String[]> reponse = parse.getParsed();
        for (String[] aReponse : reponse) {
            URL url = new URL(aReponse[5]);
            BufferedImage c = ImageIO.read(url);
            ImageIcon image = new ImageIcon(c);
            maListe.add(new Produit(aReponse[1], aReponse[2], aReponse[3], aReponse[4], aReponse[0], image));
        }
        return maListe;
    }
    public List<Rapport> getRapp(String Methode , String id , String auteur) throws Exception
    {
        this.request = "action=read&consulter="+Methode+"&auteur="+auteur+"&id="+id;
        this._sendPost();
        List<Rapport> maListe = new LinkedList<>();
        Parser parse = new Parser(answer);
        List<String[]> reponse = parse.getParsed();
        for (String[] aReponse : reponse)
        {
            maListe.add(new Rapport(aReponse[0], aReponse[1], aReponse[2], aReponse[3]));
        }
        return maListe;
    }
    public String setRapp(String auteur , String rapport , String commentaire) throws Exception
    {
        this.request = "action=write&auteur="+auteur+"&rapport="+rapport+"&commentaire="+commentaire;
        this._sendPost();
        String result ="";
        Parser parse = new Parser(answer);
        List<String[]> reponse = parse.getParsed();
        for (String[] aReponse : reponse)
        {
            result = aReponse[0];
        }
        return result;
    }

    public String[] getVisiteur(String login, String password) throws Exception {
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String mdp = (new HexBinaryAdapter()).marshal(md5.digest(password.getBytes()));
        this.request = "action=read&consulter=Visiteur&login="+ login + "&pass=" + mdp.toLowerCase();
        this._sendPost();
        Parser parse = new Parser(answer);
        List<String[]> reponse = parse.getParsed();
        if (reponse.get(0)[0].equals("mot de passe incorrect") || reponse.get(0)[0].equals("login incorrect")){
            System.out.println("Mot de passe ou Login incorrect");
            return null;
        }
        else{
            return reponse.get(0);
        }
    }
}
