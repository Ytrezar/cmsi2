package Modele;

public class Visiteur{
    private String id;
    private String nom;
    private String prenom;
    private String login;
    private String adresse;
    private Integer CP;
    private String ville;
    private String Date_embauche;
    private boolean isConnected = false;

    public Visiteur(){
    }
    public String toString(){
        return "Visiteur [id= " + this.id + ", nom= " + this.nom + ", prenom= " + this.prenom + ", login= " + this.login + ", adresse= " + this.adresse + ", CP= "+ this.CP + ", ville= "+ this.ville + ", Date d'embauche= "+ this.Date_embauche + " ]";
    }
    public void seConnecter(String login, String password) throws Exception{
        Post poste = new Post();
        String[] visiteur = poste.getVisiteur(login, password);
        if (visiteur != null){
            this.id = visiteur[0];
            this.nom = visiteur[1];
            this.prenom = visiteur[2];
            this.login = visiteur[3];
            this.adresse = visiteur[4];
            this.CP = Integer.parseInt(visiteur[5]);
            this.ville = visiteur[6];
            this.Date_embauche = visiteur[7];
            this.isConnected = true;
        }
    }
    public String getId(){return this.id;}
    public String getNom(){return this.nom;}
    public String getPrenom(){return this.prenom;}
    public String getAdresse() {return this.adresse; }
    public String getLogin() { return this.login;}
    public Integer getCP() { return this.CP;}
    public String ville() { return this.ville;}
    public String getEmbauche() { return this.Date_embauche;}
    public boolean getStatus() {return isConnected;}
}
